<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Equipo;
use app\models\Etapa;
use app\models\Maillot;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    //EJERCICIO CONSULTAS
    
    //EJERCICIO 1 
    
//-- (01) Número de ciclistas que hay
//SELECT COUNT(*) AS "numero_de_ciclistas" FROM ciclista;
    
    //Metodo DAO
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) AS "numero de ciclistas" FROM ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS "numero_de_ciclistas" FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_de_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS 'numero_de_ciclistas' FROM ciclista",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta1a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("count(*) as 'numero_de_ciclistas'"),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_de_ciclistas'],
            "titulo"=>"Consulta 1 con ORM",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS 'numero de ciclistas' FROM ciclista",
        ]);
            
    }
    
    //EJERCICIO 2
    
//-- (02) Número de ciclistas que hay del equipo Banesto
//SELECT COUNT(*) as "numero_de_ciclistas" FROM ciclista WHERE nomequipo='Banesto';
    
    //Metodo DAO
    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) as "numero_de_ciclistas" FROM ciclista WHERE nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) as "numero_de_ciclistas" FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_de_ciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) as banesto FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta2a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("count(*) as 'numero_de_ciclistas'")->where('nomequipo="Banesto"'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_de_ciclistas'],
            "titulo"=>"Consulta 2 con ORM",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) as banesto FROM ciclista WHERE nomequipo='Banesto'",
        ]);
            
    }
    
    //EJERCICIO 3
    
//-- (03) Edad media de los ciclistas
//SELECT AVG(edad) as edad_media FROM ciclista;
    
    //Metodo DAO
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT AVG(edad) as "edad_media" FROM ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as "edad_media" FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as 'edad_media' FROM ciclista",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta3a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("avg(edad) as 'edad_media'"),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 3 con ORM",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) as 'edad_media' FROM ciclista",
        ]);
            
    }
    
    //EJERCICIO 4
    
//-- (04) La edad media de los del equipo Banesto
//SELECT AVG(edad) AS 'edad_media' FROM ciclista WHERE nomequipo='Banesto';
    
    //Metodo DAO
    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT AVG(edad) AS "edad_media" FROM ciclista WHERE nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) AS "edad_media" FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS 'edad_media' FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta4a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT('avg(edad) as "edad_media"')->where('nomequipo="Banesto"'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad_media'],
            "titulo"=>"Consulta 4 con ORM",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) AS 'edad_media' FROM ciclista WHERE nomequipo='Banesto'",
        ]);
            
    }
    
    //EJERCICIO 5
    
//-- (05) La edad media de los ciclistas por cada equipo
//SELECT nomequipo,  AVG(edad) AS 'edad_media'  FROM ciclista GROUP BY nomequipo;


    
    //Metodo DAO
    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo`, avg(edad) as "edad_media" FROM `ciclista` GROUP BY `nomequipo`) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo,  AVG(edad) AS "edad_media"  FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,  AVG(edad) AS 'edad_media'  FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta5a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nomequipo, avg(edad) as 'edad_media' ")->groupBy('nomequipo'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 5 con ORM",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo,  AVG(edad) AS 'edad_media'  FROM ciclista GROUP BY nomequipo",
        ]);
            
    }
    
    //EJERCICIO 6
    
//-- (06) El número de ciclistas por equipo
//SELECT nomequipo, COUNT(nombre) AS "numero_de_ciclistas" FROM ciclista GROUP BY nomequipo;


    //Metodo DAO
    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo`, count(nombre) as "numero_de_ciclistas" FROM `ciclista` GROUP BY `nomequipo`) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(nombre) AS "numero_de_ciclistas" FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_de_ciclistas'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(nombre) AS 'numero_de_ciclistas' FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta6a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nomequipo, count(nombre) as 'numero_de_ciclistas'")->groupBy('nomequipo'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_de_ciclistas'],
            "titulo"=>"Consulta 6 con ORM",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, COUNT(nombre) AS 'numero_de_ciclistas' FROM ciclista GROUP BY nomequipo",
        ]);
            
    }
    
     //EJERCICIO 7
    
//-- (07) El número total de puertos
//SELECT COUNT(*) AS "total_de_puertos" FROM puerto;  
    
    //Metodo DAO
    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM `puerto`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS "total_de_puertos" FROM puerto',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_de_puertos'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS 'total_de_puertos' FROM puerto",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta7a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find()->SELECT("COUNT(*) AS 'total_de_puertos'"),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_de_puertos'],
            "titulo"=>"Consulta 7 con ORM",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) AS 'total_de_puertos' FROM puerto",
        ]);
            
    }
    
    //EJERCICIO 8
    
//-- (08) El número total de puertos mayores de 1500
//SELECT COUNT(*) AS "total_de_puertos" FROM puerto WHERE altura>1500;
    
    //Metodo DAO
    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from lleva WHERE código="MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS "total_de_puertos" FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_de_puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS 'total_de_puertos' FROM puerto WHERE altura>1500",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta8a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find()->SELECT("COUNT(*) AS 'total_de_puertos'")->where('altura>1500'),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total_de_puertos'],
            "titulo"=>"Consulta 8 con ORM",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) AS 'total_de_puertos' FROM puerto WHERE altura>1500",
        ]);
            
    }
    
    //EJERCICIO 9

//-- (09) Listar el nombre de los equipos que tengan más de 4 ciclistas
//SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4;  
    
    //Metodo DAO
    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo` FROM `ciclista` GROUP BY `nomequipo` HAVING count(*)>4) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta9a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nomequipo")->groupBy("nomequipo")->having("count(*)>4"),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con ORM",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
            
    }
    
    //EJERCICIO 10

//-- (10) Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32
//SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4; 
    
    //Metodo DAO
    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `nomequipo` FROM `ciclista` WHERE edad BETWEEN 28 AND 32 GROUP BY `nomequipo` HAVING count(*)>4) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta10a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->SELECT("nomequipo")->where("edad BETWEEN 28 AND 32")->groupBy("nomequipo")->having("count(*)>4"),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con ORM",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
            
    }
    
    //EJERCICIO 11

//-- (11) Indícame el número de etapas que ha ganado cada uno de los ciclistas
//SELECT dorsal, COUNT(*)  from etapa GROUP BY dorsal;  
    
    //Metodo DAO
    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `dorsal`, count(*) as "etapas_ganadas" FROM `etapa` GROUP BY `dorsal`) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, COUNT(*) as "etapas_ganadas" from etapa GROUP BY dorsal',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) as 'etapas_ganadas'  from etapa GROUP BY dorsal",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta11a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Etapa::find()->SELECT("dorsal,count(*) as 'etapas_ganadas'")->groupBy("dorsal"),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas_ganadas'],
            "titulo"=>"Consulta 11 con ORM",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) as 'etapas_ganadas'  from etapa GROUP BY dorsal",
        ]);
            
    }
    
    //EJERCICIO 12

//-- (12) Indícame el dorsal de los ciclistas que hayan ganado más de una etapa
//SELECT dorsal  from  etapa GROUP BY dorsal HAVING count(*)>1;  
    
    //Metodo DAO
    public function actionConsulta12(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM (SELECT `dorsal` FROM `etapa` GROUP BY `dorsal` HAVING count(*)>1) `c`')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal  from  etapa GROUP BY dorsal HAVING count(*)>1',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>0,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  from  etapa GROUP BY dorsal HAVING count(*)>1",
        ]);
    }
    
    //Metodo ORM
    public function actionConsulta12a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Etapa::find()->SELECT("dorsal")->groupBy("dorsal")->having("count(*)>1"),
                'pagination'=>['pagesize'=>0,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con ORM",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal  from  etapa GROUP BY dorsal HAVING count(*)>1",
        ]);
            
    }
}
